## 张亮

**电话/微信:  18698603202**

**笔记URL:  https://gitee.com/markict/ccna**

**WIFI**:  

​	SSID: Node-Student

​	PASSWD:  www.china-node.com



## 介质

**568B**

橙白  橙  绿白  蓝  蓝白   绿   棕白  棕



**568A**

绿白  绿  橙白  蓝  蓝白   橙  棕白  棕 



## IOS基本操作

**模式**

* 用户模式:    `Hostname>`  (简单的信息查看)
* 特权模式:  `Hostname#`  (所有文件的增删改查, 所有配置的查看)
* 配置模式: `Hostname(config)#`  (修改配置)



**IOS CLI特性**

* 无歧义命令补齐 (TAB)
* 无歧义命令缩写
* 命令历史记录 (上下按键) :  `Switch#terminal history size 100`  //修改历史命令数量
* 上下文帮助( ? ) :   查看命令,  查看参数 
* 错误提示

**常用快捷键**

* Ctrl + z    :  end  
* Ctrl + a   : 光标回行首
* Ctrl + e   : 光标到行尾
* Ctrl + u  : 删除光标左侧内容
* Ctrl + k  : 删除光标右侧内容



**基本命令**

```
enable   //进入特权模式
disable  //退回用户模式
configure terminal  //进入全局配置模式
exit  //退回上级模式
end  //退到特权模式

clock timezone CST +8   //在全局匹配模式下修改时区
clock set 16:11:00 12 nov  2018  //在特权模式下修改时间
show clock [detail]   //在用户/特权模式下查看时间

hostname HOSTNAME  //修改主机名


---------------------------------------
running-config   //内存中的正在运行的配置
startup-config  //nvram中的保存配置
flash:    //通常保存系统镜像文件, 和配置备份.
----------------------------------------
show running-config  //查看当前配置
show startup-config //查看保存的配置
show flash:  //查看flash:中的文件

copy running-config startup-config  //保存配置(特权模式)
write  //等同于copy running-config startup-config
reload  //重启设备

show running-config | include xxx  //过滤包含xxx的那一行
show running-config | section RE //过滤包含RE的那个配置块

erase startup-config  //清除保存的配置
reload  //重启

show interfaces //查看设备接口详细信息
show interfaces description  //查看接口简要描述信息

interface e0/0  //在全局配置模式下进入接口子配置模式
	shutdown  //关闭接口
	no shutdown //打开接口
	description DESC //为接口添加描述信息
	duplex {full | half | auto }  //更改双工模式
	speed {1000 | 100 | 10 | auto}  //更改接口速率

interface range e0/0-3   //批量操作连续接口
interface range e0/0,e0/2   //批量操作不连续接口

interface e0/0
	no shutdown
	ip address 172.16.1.13 255.255.255.0  //给三层接口配置IP
ping 172.16.1.4  [repeat N | timeout N | source ADDR]


show mac address-table //查看交换机mac 地址表
clear mac address-table dynamic  //清除mac地址表
mac address-table aging-time N  //修改mac表老化时间

interface vlan 1   //为交换机创建一个SVI接口
	no shutdown 
	ip address 172.16.1.5 255.255.255.0
interface loopback 0  //为设备创建一个环回接口
	ip address 172.16.3.3 255.255.255.255
    
show ip interface brief //查看接口的三层地址

```



| 区域  | 网络ID        | 子网掩码             |
| ----- | ------------- | -------------------- |
| 蓝色  | 192.168.1.0   | 255.255.255.128  /25 |
| 黄色  | 192.168.1.192 | 255.255.255.224  /27 |
| 绿色  | 192.168.1.128 | 255.255.255.192  /26 |
| R1-R2 | 192.168.1.224 | 255.255.255.252  /30 |
| R1-R3 | 192.168.1.228 | 255.255.255.252 /30  |
| R2-R3 | 192.168.1.232 | 255.255.255.252 /30  |



### Telnet

```
Server Config
#使用线路下密码进行身份认证
line vty 0 2  //创建三个虚拟终端线路 (允许三个用户同时登录)
	transport input telnet //允许TELNET协议远程登录
	password  cisco123  //设置线路下密码,进行登录认证
	login  //启用线路下密码进行登录认证
-----------------------------------------------		
#使用全局模式的本地用户进行登录认证
username alice password 123  //创建一个普通用户
username bob privilege 15 password 456 //创建一个特权用户

line vty 0 2
	transport input telnet
	login local  //使用本地用户进行登录认证
	
-----------------------------------------------	
Client:
telnet 34.1.1.5  //telnet登录远程设备

----------------------
who  //在被登录设备上查看登录用户
clear line N //踢出指定线路的用户

Ctrl + Shift + 6 ;  x   //在客户端设备切到本设备的shell
show session  //查看客户端设备登录了哪些远程设备
disconnect SESS_ID  //断开与远程设备的连接
```



## 路由基础

### 选路规则 

* 优先使用匹配精确的路由条目
* 匹配精度同样时, 优先使用管理距离(AD)小的路由.  
* 管理距离相同, 优先使用度量值(Metric)小的路由.
* 以上全部相同, 多条路径同时使用(负载均衡)



**CDP**

```
show cdp neighbors [detail]  //查看邻居设备的信息
show cdp traffic  //查看CDP数据包的统计信息
show cdp interface //查看接口关于CDP的配置信息
[no] cdp run //全局打开或关闭CDP功能
interface e0/0
	[no] cdp enable //打开或关闭指定接口的CDP功能
```

*lldp 是一个公有标准, 用于完成邻居发现的功能(思科设备默认关闭状态)*

**ARP**

```
arp -a  //在Win/Linux/MacOS上查看ARP缓存表
show ip arp  //IOS上查看ARP缓存表
clear ip arp  ADDR //手工清除ARP缓存 
arp  10.1.1.88 0000.8888.8888  arpa   //手工添加APR条目
```

**路由器模拟主机**

```
no ip routing //关闭路由功能
ip defaut-gateway 10.1.1.254  //为模拟主机的路由器设置网关
```





**Static Route**

```
ip route NETWORK MASK {Next-Hop | Out-IF} [distance] [permanent] 

ip route 172.16.1.0 255.255.255.0  10.1.1.3 
ip route 172.16.1.0 255.255.255.0 s0/0
ip route 172.16.1.0 255.255.255.0 10.1.1.3 s0/0 

show ip route  //查看路由表中的路由条目
```

*distance: 指定本条静态路由的AD值*

*permanent: 如果不指定该参数 , 那么静态路由下一跳地址不可达时, 该路由不出现在路由表中,  如果指定该参数, 无论下一跳是否可达, 该条路由都会出现在路由表*

```
ping DEST_ADDR  [source {ADDR|IF}] [repeat N]
traceroute DEST_ADDR [numeric]  
debug ip icmp  //查看icmp报文
debug ip packet  //查看ip报文
undebug all  //关闭所有debug
```

**作业**:

​	自学习一下traceroute的工作原理



## VLAN

**基础配置**

```
vlan  10  //创建vlan 10
	name Sales  //为vlan命名
	
vlan 20,30,40 //批量创建vlan

interface e0/0
	switchport mode access //设置接口为access模式
	switchport access vlan 10  //接口划入vlan 10

show vlan [brief] //查看vlan信息
show interface e0/1 switchport  //查看接口和vlan相关的信息
```

**Trunk**

```
interface e1/0  
	switchport trunk encapsulation dot1q  //设置封装协议为dot1q
	switchport mode trunk //设置为trunk模式
	switchport native vlan  99 //设置trunk的native vlan (可选)
	switchport trunk allow vlan {add | remove | all | VLAN_ID}  //设置trunk允许通过的vlan
	
show interface trunk // 查看该交换机上的trunk接口
show interface e1/0 switchport //查看该接口vlan相关信息
vlan  dot1q tag native  //给native vlan打标签
```

`switchport nonegotiate`  //在接口上关闭DTP协商

**VTP**

```
interface e1/0
	switchport trunk encapsulation dot1q  
	switchport mode trunk    //交换机互联接口配置为trunk

vtp domain cciex.com  //配置vtp域
vtp mode  server/client/transparent
vtp password  123   //配置vtp密码

show vtp status  //查看vtp信息
show vtp password  //查看VTP密码
```

*server/client的vtp信息和vlan信息全部保存在flash:/vlan.data中*



**STP**

```
show spanning-tree  //查看生成树

spanning-tree vlan 1 priority 0  //设置优先级
spanning-tree vlan 2 priority 4096  
#另一方式
spanning-tree vlan 1 root primary 
spanning-tree vlan 2 root secondary

spanning-tree portfast edge   //为接入主机的接口设置portfast 
spanning-tree portfast edge  trunk  //为接入主机的trunk启用portfast

interface e0/0
	spanning-tree vlan 1 cost  2000 //修改指定接口的 COST
	spanning-tree vlan 1 port-priority  64 //修改接口优先级


```



## PortSecurity

```
 switchport mode access  //设置access模式
 switchport port-security maximum 2  //设置最大绑定mac数量
 switchport port-security violation {protect|restrict|shutdown}  //设置违规操作模式
 switchport port-security mac-address sticky  //自动绑定
 switchport port-security mac-address 0000.1111.1111  //手工绑定
 switchport port-security aging time 5  //老化时间
 switchport port-security aging type inactivity  //老化类型
 switchport port-security aging static  //允许老化静态mac
 switchport port-security  //启用端口安全功能
 
 show port-security interface e0/0  //查看接口端口参数 
 
 errdisable recovery cause psecure-violation  //由于端口安全导致的error-disable 可以做自动恢复
 errdisable recovery interval  180   //每隔180s进行一次恢复
```

## SPAN

**span**

```
monitor session 1 source interface e0/0 {rx|tx|both}
monitor session 1 destination interface e0/1 
```

**rspan**

```
1.  交换机之前必须是trunk
2.  vlan 88
		remote-span  //创建远程vlan 
3.  源交换上配置:
	monitor session 1 source interface e0/0 {rx|tx|both}
	monitor session 1 destination remote vlan 88 
4.  目的交换配置:
	monitor session 1 source remote vlan 88
	monitor session 1 destination interface e0/1  

```

**EtherChannel**

```
interface range e0/1,e0/3 
	channel-group 1 mode {on|auto|desir|active|passive}

port-channel load-balance {src-ip|dst-ip|src-mac|dst-mac|src-dst-ip|src-dst-mac}  

interface port-channel 1 
	xxxxxxx   //建立port-channel后,所有操作在port-channel中进行
	
show etherchannel summary   //查看 etherchannel 信息
```



## FHRP

**HSRP**

```
interface e0/0
	ip address 10.1.1.1 255.255.255.0
	standby 10 ip 10.1.1.254 //配置vIP
	standby 10 priority  200 //配置优先级
	standby 10 preempt  //开启抢占功能

show standby  //查看HSRP状态信息

在测试主机上:
1.  ip route 0.0.0.0 0.0.0.0 10.1.1.254 
2.  no ip routing 
	ip default-gateway 10.1.1.254
```

**VRRP**

```
interface Ethernet0/0
 ip address 10.1.1.1 255.255.255.0
 vrrp 10 ip 10.1.1.254
 vrrp 10 timers advertise msec 500
 vrrp 10 priority 200
 vrrp 10 preempt  

show vrrp 
```

**GLBP**

```
interface Ethernet0/0
 ip address 10.1.1.1 255.255.255.0
 glbp 10 ip 10.1.1.254   //设置vIP
 glbp 10 priority 200   //设置优先级
 glbp 10 preempt   //开启抢占功能
 glbp 10 weighting 100 lower 60 upper 80  //设置权重参数 
 glbp 10 load-balancing weighted    //设置负载模式为: 基于权重负载
```

## RIP

```
router rip
	version 2  //使用RIPv2
	no auto-summary  //关闭自动汇总功能
	network 12.0.0.0
	network 1.0.0.0   //通告接口
	passive-interface e0/3 //设置该接口为被动接口
	
show ip protocols  //查看运行的路由协议
show ip route rip  //查看路由表中rip的路由条目

```



## RIPng

```
ipv6 unicast-routing  //全局开启IPv6的路由功能

interface e0/0  
	ipv6 enable  //接口启用v6, 自动生成Link-local地址
	ipv6 address fe80::1 link-local //手工指定Link-local地址
	ipv6 address 2011::1/64  //手工配置ipv6 Global地址
	ipv6 address 2011::/64 eui-64  //使用EUI64自动配置地址
	
ipv6 router rip TAG  //创建RIPng进程,指定进程标记

interface e0/0
	ipv6 rip TAG enable //通告接口到指定的RIPng进程

show ipv6 interface brief  //查看接口v6地址
show ipv6 route  //查看v6路由表


```





## OSPF

```
router ospf 1
 router-id 1.1.1.1   //手工指定RID
 auto-cost reference-bandwidth 10000  //设置参考带宽
 passive-interface Ethernet0/3   //设置被动接口
 network 1.1.1.1 0.0.0.0 area 0    //通告接口到OSPF区域0
 network 12.1.1.1 0.0.0.0 area 0

interface e0/0
	ip ospf cost   88  //手工指定接口的ospf Cost

show ip ospf neighbor   //查看OSPF邻居关系
show ip ospf database //查看LSDB
show ip route ospf  //查看路由表中的OSPF的条目
show  ip ospf interface e0/0  //查看指定接口的OSPF相关信息

```

## EIGRP

```
router eigrp 100   //邻居之间AS号要一致
 network 1.1.1.1 0.0.0.0   
 network 12.1.1.1 0.0.0.0
 network 13.1.1.1 0.0.0.0
 network 14.1.1.1 0.0.0.0   //通告接口
 passive-interface Ethernet0/3   //设置被动接口
 eigrp router-id 1.1.1.1   //设置EIGRP  RID
 variance 2   //设置非等价负载均衡
 
show ip eigrp neighbors  //查看eigrp邻居
show ip eigrp topology [all-links]  //查看eigrp的拓扑表
show ip route eigrp  //查看路由表中eigrp的条目

interface e0/1
	delay  150  //修改接口延时为1500ms
	bandwidth 100  //修改带宽为100kbps
	ip hello-interval eigrp 100 2   //修改hello间隔为2s
	ip hold-time eigrp 100 6   //修改hold时间为6s
```



## PPP

```
-----PAP认证-----
Server:
username alice password 123  //配置本地用户名和密码,用于认证
interface Serial0/0
 ip address 12.1.1.2 255.255.255.0
 encapsulation ppp   //设置链路层协议为PPP
 ppp authentication pap   //使用PPP方式进行认证 (明文)
 
Client:
!
interface Serial0/0
 ip address 12.1.1.1 255.255.255.0
 encapsulation ppp
 ppp pap sent-username alice password 123  

-----CHAP认证-----
Server:
username alice password 123  //配置本地用户名和密码,用于认证
interface Serial0/0
 ip address 12.1.1.2 255.255.255.0
 encapsulation ppp
 ppp authentication chap  //使用CHAP认证
 
Client:
interface Serial0/0
 ip address 12.1.1.1 255.255.255.0
 encapsulation ppp
 ppp chap hostname alice
 ppp chap password 123
```

**MLP**

```
interface Multilink1   //创建一个虚拟接口
 ip address 12.1.1.1 255.255.255.0
 ppp multilink   //启用MLP
 ppp multilink group 1   //组号码为 1
 
 interface Serial0/0
 no shutdown
 encapsulation ppp
 ppp multilink
 ppp multilink group 1  //加入组1
 
 interface Serial0/1
 no shutdown
 encapsulation ppp
 ppp multilink
 ppp multilink group 1	//加入组1

show ppp multilink  //查看multilinke信息
```

*default interface  s0/0 :  快速清除接口下的配置(恢复默认配置)*

## DHCP

```
DHCP:
ip dhcp excluded-address 10.1.1.1 10.1.1.20   //排除地址
ip dhcp pool CCNA   //创建DHCP地址池
 network 10.1.1.0 255.255.255.0  //地址池中分配的地址范围
 default-router 10.1.1.4  //默认网关
 dns-server 10.1.1.4  //DNS服务器
 domain-name cciex.com  //默认域
 lease 0 2 20   //租期
 
show ip dhcp  pool   //查看地址池信息
show ip dhcp binding //查看地址分配信息

DHCP Relay:
interface e0/1
	ip helper-address 10.1.1.3  //在中继设备接口设置DHCP服务器的地址.
 
 DNS:
ip dns server   //IOS启用DNS服务
ip host www.cciex.com 5.5.5.5    //添加解析记录
ip host www.cisco.com 55.55.55.55
ip host ftp.cciex.com 45.1.1.5

Client:
no ip routing  //路由器模拟主机, 关闭路由功能
interface e0/0
	no shutdown 
	ip address dhcp //通过DHCP自动获取地址

```



## Access-list

**编号的标准ACL**

```
access-list 1 permit 172.16.1.4
access-list 1 permit 172.16.1.3
access-list 1 deny   172.16.0.0 0.0.255.255
access-list 1 permit 10.1.1.0 0.0.0.255

show access-list   //查看条目
```

**命名的标准ACL**

```
ip access-list standard ACL1
 permit host 172.16.1.5
 permit host 172.16.1.3
 deny   172.16.0.0 0.0.255.255
 permit any

ip access-list standard ACL1
  15 permit host 172.16.1.4   //插入一条
  no  30  //删除指定条目

interface e0/0
	ip access-group ACL1  in/out   //接口下调用ACL

line vty 0 4 
	access-class ACL1 in  //在vty下调用ACL, 对远程登录进行限制 

show ip access-list  //查看ACL
ip access-list resequence ACL1  10  20  //对ACL进入重新排序 
```

**扩展ACL**

```
ip access-list extended ACL_NAME
	permit/deny PROTOCOL  Source_Prefix Wildcard_Mask [eq| lt|gt SOURCE_PORT]  Destination_Prefix Wildcard_Mask [eq|lt|gt Dest_PORT] [Other_Pram] 




ip access-list extended ACL1
 permit tcp host 1.1.1.1 host 3.3.3.3 eq telnet
 permit icmp host 1.1.1.2 host 33.33.33.33 echo
 deny   ip any any
 
interface e0/0
	ip access-group ACL1  in/out   //接口下调用ACL
	
show ip access-list  //查看ACL
```



## NAT

**动态NAT**

```
interface Ethernet0/0
 ip address 12.1.1.2 255.255.255.0
 ip nat inside  //定义接口为NAT的inside
 
interface Ethernet0/1
 ip address 23.1.1.2 255.255.255.0
 ip nat outside   //定义接口为NAT的outside
 
ip access-list standard NAT_Source  //ACL定义需要转换的源地址
 permit 1.1.1.0 0.0.0.3

ip nat pool POOL1 23.1.1.11 23.1.1.20 netmask 255.255.255.0  //定义公有地址池

ip nat inside source list NAT_Source pool POOL1  //定义转换规则 

show ip nat translations  //查看NAT转换表
debub ip nat  //debug转换过程
```

**Static NAT**

`ip nat inside source static 1.1.1.1 23.1.1.111 ` 

**Dynamic PAT**

```
ip nat inside source list NAT_Source pool POOL1 overload   //使用地址池中的公有地址
ip nat inside source list NAT_Source interface e0/1 overload  //使用公网接口地址转换
```

**Static PAT**

```
ip nat inside source static tcp 1.1.1.1 80 interface Ethernet0/1 80   //映射内网1.1.1.1的80端口到接口e0/1的80端口.

ip nat inside source static tcp 1.1.1.2 23 interface Ethernet0/1 2323   //映射内网1.1.1.2的23端口到接口e0/1的2323端口

ip nat  inside source static tcp 1.1.1.3 23 23.1.1.33 23 
```

## Logging

**Console Line**

`[no] logging console`  //打开或关闭console日志

**VTY **

```
who   //确定自己所在的vty号码 
line vty 0
	monitor   //在vty0下开启日志监控
```

**Buffer**

```
logging buffered 40960 5   //创建一个40960B的日志缓存, 缓存 5级及以上日志
show logging  //查看缓存日志
```

**Syslog Server**

```
logging host 10.70.2.55 [transport udp  port 514]   //设置日志服务器
logging trap errors   //发送3级及以上日志到server
```



### SNMP

```
snmp-server community 123 RO   //定义只读community
snmp-server community 456 RW    //定义读写community
snmp-server location Dalian,CN   
snmp-server contact Liang,18698603XXX
snmp-server host 10.70.2.58 ro  ospf eigrp  //定义trap server
```



## NTP

```
Server:
ntp authentication-key 1 md5 cisco123  //设置密钥
ntp authenticate  //启用认证
ntp trusted-key 1   //选取密钥
ntp master 3   //启用NTP 服务

Client:
ntp authentication-key 1 md5 cisco123
ntp authenticate
ntp trusted-key 1
ntp server 12.1.1.2 key 1   //指定NTP服务器
show ntp status   //查看NTP同步状态

```



## IOS Device Management

```
verify /md5 flash:/c2900-universalk9_npe-mz.SPA.15xxx.bin //计算IOS的MD5哈希值.
boot system flash:/c2900XXX.bin  //设置引导镜像

```

**密码恢复**

```
1. 开机按Break 进入ROMMON模式
2. confreg 0x2142  //修改配置寄存器, 忽略startup-config
3. copy startup-config running-config
4. 重置密码
5. config-register 0x2102  //改回配置寄存器值
6. write  //保存配置
7. reload  //重启设备
```



**SSH**

```
Server:
crypto  key generate rsa label SSH_KEY modulus 1024   //创建一对RSA密钥
show crypto key  mypubkey rsa   //查看RSA公钥
crypto key export rsa SSH_KEY pem url unix: 3des CISCO123  //将 RSA 密钥导出,进行备份.
crypto key import rsa SSH_KEY  url unix:SSH_KEY  cisco123  //导入指定密钥
crypto key zeroize rsa SSH_KEY   //清除指定密钥



username alice secret 123    //创建认证用户
username bob privilege 15 secret 456  

line vty 0 4
 login local    //使用本地用户认证
 transport input ssh   //允许SSH协议
 
 Client:
 ssh -l bob 12.1.1.2   //IOS设备作为客户端,SSH远程程登录
 ssh bob@12.1.1.2  //Unix/Linux可以简化
 
```

**Banner**

`banner login  XXXX`   //登录前的banner

`banner exec XXX`  //登录后的banner





## DHCP_SNOOPING

```
ip dhcp snooping   //开启dhcp snooping
ip dhcp snooping vlan 1   //基于vlan开启dhcp snooping
no ip dhcp snooping information option  //关闭option82信息添加
ip dhcp snooping verify mac-address  //检查mac和chaddr一致性
ip dhcp snooping database unix:dhcp.db   //DB保存路径
ip dhcp snooping database write-delay 30  //备份周期
ip dhcp snooping database timeout 10  //写入超时

interface e0/0
	ip dhcp snooping trust  //设置接口为信息接口
interface e0/1
	ip dhcp snooping limit rate  10 //设置dhcp速率限制

show ip dhcp snooping binding  //查看dhcp snooping的绑定数据记录



SW2#ip dhcp  snooping binding 0000.1111.1111 vlan 1 10.1.1.11 interface  e0/3 expiry 4294967295    //手工在DB中增加条目.
```

**DAI**

```
ip arp  inspection vlan 1   //vlan1开启DAI功能
interface e0/0
	ip arp inspection trust  //设置接口为信任接口
interface e0/1
	ip arp  inspection limit rate 10 //为非信任接口设置arp报文限速
```

**IPSG**

```
interface e0/0
	ip verify source port-security   //接口下开启IPSG功能
```



### AAA

```
username alice password 123  //创建本地用户

aaa new-model  //开启AAA

tacacs server TS1  //定义一个AAA tacacs+ server
 address ipv4 10.1.1.11   //server地址
 key 123   //对接密钥
 
tacacs server TS2
 address ipv4 10.1.1.12
 key 123
 
aaa group server tacacs+ TG   //定义一个aaa server组
 server name TS1   //添加aaa server到组
 server name TS2
 
aaa authentication login  VTY_Authen  group TG local  //创建一个认证列表,名称为VTY_Authen,  方法顺序为 先用TG组认证, 如果TG故障,使用本地认证.
aaa authentication login CON_Authen none   //创建另一个认证列表,名为CON_Authen, 不进行认证

aaa authorization exec VTY_Author group TG  //创建一个级别授权列表
aaa accounting exec VTY_Acc start-stop group TG //创建一个审计列表

line con 0
 login authentication CON_Authen  //调用认证列表 CON_Authen
 
line vty 0 4
 login authentication VTY_Authen  //调用认证列表 VTY_Authen
 transport input telnet
 authorization exec  VTY_Author   //调用级别授权列表
 accounting exec VTY_Acc   //调用审计列表
```

