## Syslog

**Console line**

`[no] logging console`   //打开或闭关console的日志显示

**Terminal line**

```
who  //通过who查看自己所在的vty线路
line vty 0  //进入自己所在线路
	monitor  //打开terminal line上的log功能
```

**logging buffer**

```
logging buffered 409600 5  //设置日志缓存区大小, 以及缓存级别
show logging //查看缓存区的日志
```

**log server**

```
logging host 8.8.8.8 transport udp port 514 //设置log server参数 
logging trap 5  //5级及以上日志发送到log server
```

*推荐软件 < Kiwi syslog server> *

## SNMP

```
snmp-server location Dalian,Liaoning,CN
snmp-server contact ZhangLiang,18698603202
snmp-server community cisco123 RO  //定义一个只读权限的团体符
snmp-server community cisco456 RW  //定义一个读写权限的团体符
snmp-server host 8.8.8.8 cisco123  bgp ospf  //设置trap
```



**密码恢复**

```
1. 开机按Break 进入ROMMON模式
2. confreg 0x2142  //修改配置寄存器, 忽略startup-config
3. copy startup-config running-config
4. 重置密码
5. config-register 0x2102  //改回配置寄存器值
6. write  //保存配置
7. reload  //重启设备
```

**升级系统**

```
boot system flash:/c2900-xxxx.bin  //指定启动时加载的系统文件
verify /md5 flash:c3560-ipservicesk9-mz.122-55.SE12.bin  //计算系统文件的HASH
```



## GRE

```
1. 使用静态默认,保证站点边界设备公有地址互通
2. 在站点边界设备上配置GRE Tunnel
	interface tunnel 0 
		tunnel mode gre ip  //设置tunnel的模式为GRE
		tunnel source 23.1.1.2  //设置tunnel源
		tunnel destination 34.1.1.4  //设置tunnel的目的
		ip address 24.1.1.2 255.255.255.0  //为tunnel 接口配置地址

3. 通过静态路由, 进行站点间主机互通
```

