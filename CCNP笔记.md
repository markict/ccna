# 信息

##张亮

**Tel/Wechat: 18698603202**

**CCIE#37812  HCIE#2189**

**RS  Security  Virtualization  Cloud  BigData**

## WIFI

**SSID:** Node-Student

**Password:** www.china-node.com

**Note_URL:  https://gitee.com/markict/ccna**



# 笔记

## GRE Tunnel

**配置**

```
interface tunnel 0     //创建一个隧道接口
	tunnel mode gre ip //更改隧道模式为GRE封装
	tunnel source  12.1.1.1  //设置隧道源端地址
	tunnel destination 23.1.1.3  //设置隧道的对端地址
	ip address  13.1.1.1 255.255.255.0  //设置隧道接口的ip地址
	
```

*注意: 一定要保证tunnel的source和destination的连通性*

**推荐实验步骤**

* 配置路由器直连接口地址, 并验证连通性.
* 配置静态路由, 保证边界路由器公有地址之间的连通性.
* 配置GRE Tunnel, 保证两台边界路由器tunnel接口之间的连通性.
* 配置EIGRP路由协议,保证端到端的连通性.



## RIPng

```
ipv6 unicast-routing    //启用ipv6路由协议功能
ipv6 router rip CCNP   //创建一个RIPng路由进程, 标签为CCNP

interface loopback 0 
	ipv6 address 2011::1/128
	ipv6 rip CCNP enable  //宣告到RIPng
interface E0/0
	no shutdown 
	ipv6 enable   //接口启用ipv6, 会自动生成link-local地址
	ipv6 rip CCNP enable  //宣告接口到RIPng
	ipv6 rip CCNP default-information originate   //向与该接口相连的RIPng路由器发放默认路由

show ipv6 route  //查看IPv6路由表
show ipv6 interface brief  //查看IPv6接口地址和状态

```



## EIGRP

**基础配置**

```
router eigrp 100 
	eigrp router-id 1.1.1.1
	network 12.1.1.1 0.0.0.0   //通告接口到EIGRP
	network 0.0.0.0 0.0.0.0   //通告所有接口到EIGRP(一般不建议)
	passive-interface  e0/0  //将e0/0接口设置为被动接口
	metric weights 0 0 0 1 0 0   //修改EIGRP的K值
	
show ip eigrp neighbors  //查看eigrp邻居关系
show ip eigrp interface [detail]  //查拉接口上和eigrp相关的信息
show ip protocols  //查看路由器上运行的路由协议相关参数 

router eigrp 100
	passive-interface default   //将所有接口设置为被动接口
	no passive-interface e0/1   //将e0/1设置为非被动接口

debug eigrp packet hello [update | query | reply | ack ] [detail]
undebug all //关闭所有debug 



interface e0/0
	ip hello-interval eigrp 100 3  //设置hello时间为3秒
	ip hold-time eigrp 100 9  //设置hold时间9秒

show ip eigrp traffic  //查看eigrp报文的统计信息
show ip route eigrp  //查看路由表中的eigrp路由条目
show ip eigrp topology [all-links]  //查看EIGRP拓扑表

```

### 概念

```
RD:  报告距离,  邻居路由器到达目标的metric.
CD:  计算距离,  本路由器到邻居的metric 加上RD(本路由器到达目标的总距离)
FD:  可行距离,  最小的CD
S :  后继,  最优路径
FS:  可行后续, 符合FC(可行条件)的路径
FC:  可行性条件,  次优路径的RD距离要小于S的FD距离.
```

* 环路路由的RD一定比S的FD大
* RD比S的FD小的路径一定不是环路
* RD比S的FD大的路径不一定是环路



### Metric

**metric=[K1\*bandwidth+(K2\*bandwidth)/(256–load)+K3\*delay]\*[K5/(reliability+K4)]**

delay   单位是10ms   bandwidth 单位是kb/s



### Stub Router

* receive-only :    仅收路由, 不发任何路由  `eigrp stub receive-only `
* connected:    仅发直接路由,其它不发 `eigrp stub connected`
* summary:  仅发汇总路由,其它不发 `eigrp stub summary`
  * `ip summary-address eigrp 100  2.2.2.0 255.255.255.0` *接口下命令*
* static:  仅发重分布进eigrp中的静态路由,其它不发 `eigrp stub static`
  * `redistribute static metric 100000 100 255 1 1500`  重分布静态到eigrp进程,并指定metric

* redistributed :  仅发重分布进来的路由, 其它路由不发 `eigrp stub redistributed`



### EIGRP Summary

**Auto-Summary**:    

```
router eigrp 100
	auto-summary    //开启自动汇总
```

*只在主类网络边界进行汇总*

**手工汇总**

```
interface e0/0
	ip summary-address eigrp 100 10.1.0.0 255.255.0.0   
```

**实验**

R2:      10.1.1.2 /24     10.1.2.2 /24      10.1.0.2/24

R3:      10.2.1.3 /24     10.2.2.3 /24      10.1.3.3/24 



### Eigrp Default route

**重分布静态默认**

```
ip route 0.0.0.0 0.0.0.0  45.1.1.5
router eigrp 100
	redistribute static 1 1 1 1 1   
```

**使用主类网络做为默认路由**

```
interface loopback 8
	ip address 192.168.8.8 255.255.255.0   
ip default-network  192.168.8.0    
```

*一定要用主类网络*



### Eigrp loadbalance

**等价负载**

* 两条(或以上)路径的Metric一样, 自动负载.

* ```
  router eigrp 100
  	maximum-paths 4   //设置最多负载路径数量
  ```

**非等价负载**

```
router eigrp 100
	variance  2 
```

*只有FS有资格参与非等价负载*

*非等价负载,是按metric反比例进行负载*



### Eigrp for IPv6

```
interface loopback  0
	ipv6 address 2022:1::2/64  
	ipv6 eigrp 100  //宣告接口到eigrp 100
interface e0/0
	ipv6 enable
    ipv6 eigrp 100  //宣告接口到eigrp 100
	
ipv6 unicast-routing   //启用v6路由协议支持
ipv6 router eigrp 100   //启用EIGRP for IPv6
	eigrp router-id 1.1.1.1    //设置RID
	
interface e0/2
	ipv6 summary-address eigrp 100  2022::/29  //接口做路由汇总
```





ip routing 

ipv6 unicast-routing



### Named Eigrp

```
router eigrp CCNP
 !
 address-family ipv4 unicast autonomous-system 100
  !
  af-interface Ethernet0/0    //进入地址族中的接口配置模式
   summary-address 10.1.0.0 255.255.0.0   //汇总
   hello-interval 6      //更改接口的hello时间
   hold-time 16
  exit-af-interface
  !
  topology base         //能做很多配置, 如重分布
  exit-af-topology
  network 10.1.1.2 0.0.0.0   //宣告接口
  network 10.1.2.2 0.0.0.0
  network 10.1.3.2 0.0.0.0
  network 12.1.1.2 0.0.0.0
  eigrp router-id 2.2.2.2   //配置RID
 exit-address-family
 !
 address-family ipv6 unicast autonomous-system 100   //V6地址族
  !       
  af-interface Loopback0
   shutdown     //不通告该接口的意思 (v6默认通告所有接口)
  exit-af-interface
  !
  topology base
  exit-af-topology
 exit-address-family
!
```



### Eigrp Authentication

```
key chain KC
 key 1
  key-string cisco123
  accept-lifetime 10:00:00 Nov 10 2018 infinite
  send-lifetime 10:00:00 Nov 10 2018 20:00:00 Nov 15 2018
 key 2
  key-string cisco456
  accept-lifetime 20:00:00 Nov 14 2018 infinite
  send-lifetime 10:00:00 Nov 15 2018 infinite
 
!
interface Ethernet0/0
 ip address 45.1.1.5 255.255.255.0
 ip authentication mode eigrp 100 md5
 ip authentication key-chain eigrp 100 KC
 
 
 
​`````````````````````````````````
router eigrp CCNP
 !
 address-family ipv4 unicast autonomous-system 100
  !
  af-interface Ethernet0/0
   authentication mode md5
   authentication key-chain KC
  exit-af-interface
```

* 本端和对端相同Key ID的key 的发送周期要和对端的接受周期吻合. 本端的接受周期要和对端的发送周期吻合.
* 同一个Key Chain相邻的Key之间, 周期要有重合, 保证平滑切换







# OSPF

```
interface e0/0
	ip ospf priority  100  //修改接口ospf优先级为100 (default: 1)
	ip ospf hello-interval  5  //修改接口hello时间
	ip ospf dead-interval  15  //修改接口dead时间
	auto-cost reference-bandwidth 10000  //修改参考带宽
	
	
router ospf 1
	network 45.1.1.4 0.0.0.0 area 0 
	neighbor 45.1.1.5  //在NBMA网络下, 需要单播指定OSPF邻居.
	ip ospf cost  5  //修改开销值
	
	
show ip ospf interface e0/0  //查看指定接口和OSPF相关的信息

interface e0/0
	ip ospf network  broadcast |  point-to-point | ...  //更改接口的OSPF网络类型
	
router ospf 1 
	area 0 range 192.168.0.0 255.255.252.0   //对area 0的路由进行汇总.
	default-information originate [always]  //下发默认路由
	
```



**LSA相关**

```
show ip ospf database  //查看LSDB简要信息
show ip ospf database router  //查看1类lsa
show ip ospf database network  //查看2类LSA
show ip ospf database summary  //查看3类LSA
show ip ospf database asbs-summary  //查看4类LSA
show ip ospf database external  //查看5类LSA

```



**Virtual-link**

```
router ospf 1
	area 1 virtual-link   4.4.4.4   //跨越区域1 创建一个v-link. 对端路由器的RID是4.4.4.4
	
```

* virtual-link不会定期发送LSA
* 经过v-link传递的LSA不会老化

**Stub Area**

```
router ospf 1
	area 1 stub   //设置为普通末节区域

router ospf 1 
	area 1 stub no-summary  //在ABS上设置,过滤3,4,5类LSA (完全末节)
	
router os 1 
	area 2 nssa   //设置非纯末节区域
	area 2 nssa default-information-originate  //ABR手工下放默认路由

router os 1 
	area 2 nssa no-summary  //设置区域为 完全的非纯末节区域.
```

*虚拟链路穿越的区域不可以设置为末节区域*



**OSPFv3**

```
ipv6 unicast-routing  //启用v6路由协议功能
router ospfv3  1
	router-id 1.1.1.1
	address-family ipv6 unicast 
		summary-address ...
		passive-interface ...
		max-path ...
		distance ...

interface e0/0
	ipv6 enable  //为接口生成link-local地址
	ospfv3 1 ipv6 area 0  //通告接口

show ospfv3 ipv6 neighbors  //查看ipv6 ospf邻居关系
....

```



# 重分布

**所有路由协议重分布到OSPF, 会有默认的metric (也可以手工指定)**

```
router ospf 1 
	redistribute eigrp 100 [subnets]  //距离矢量路由重分布到OSPF时, 在某些版本IOS上需要加subnets参数, 以便允许将子网络重分布.
	redistribute eigrp 100 [metric 33]  [metric-type 1|2 ]   //OE1 累加metric    OE2不累加Metric
	
```



**所有路由协议重分布到距离矢量型路由协议时, 没有默认metric, 需要设置.**

```
router eigrp 100
	default-metric 1000 100 255 100 1500 //设置默认metric (方式一)

router eigrp 100
	redistribute ospf 100 metric 1000 100 255 100 1500 //重分布时指定metric 
```

**ospf的外部路由重分布到BGP**

```
router bgp 1
	redistribute ospf 100 match  external 2   //手工match一下外部路由

```



## Filtering

**Distribute List**

```
ip access-list standard FilterR1.acl
 deny   1.1.2.0
 permit any

router eigrp 100
 distribute-list FilterR1.acl in Ethernet0/0
 network 12.0.0.0

```

**Prefix List**

```
ip prefix-list  PRE deny  1.1.1.0/24   //匹配子网掩码长度为24的前缀为1.1.1.0的路由条目

ip prefix-list PRE deny 172.16.0.0/16 ge 24 //匹配路由前缀前16位和172.16.0.0一样的,并且子网掩码长度大于等于24的路由条目.

ip prefix-list PRE permit 192.168.0.0/16  le 28  //匹配路由前缀前16位和192.168.0.0一样的, 并且子网掩码长度大于等于16  小于等于28的路由条目.

ip prefix-list PRE deny 10.1.0.0/16 ge 24 le 30  //....
```

```
ip prefix-list FilterR1.pre seq 5 deny 1.1.2.0/24
ip prefix-list FilterR1.pre seq 10 permit 0.0.0.0/0 le 32

```

**OSPF区域间过滤**

```
router ospf 1
 area 0 range 1.1.2.0 255.255.255.0 not-advertise
```

**重分布过滤**

```
router eigrp 100
 distribute-list prefix FilterR1.pre out ospf 1  //过滤通过重分布来自OSPF的指定路由
 network 23.0.0.0
 redistribute ospf 1 metric 1 1 1 1 1
```



**Route-Map**

```
#定义一个Route-Map

route-map FilterCon deny 10
 match interface Loopback2   //还能match acl/prefix/tag/cost
route-map FilterCon permit 20
 match interface Loopback1
 set metric 888 888 1 1 1   //为匹配路由设置metric
 set tag 666		 //为匹配路由设置路由tag
route-map FilterCon permit 30   //没有match语句,表示match any

#方式一:
router eigrp 100
	redistribute connected route-map FilterCon   //在重分布时调用Route-Map进行过滤

#方式二:
router eigrp 100
	redistribute connected   //正常重分布, 不用过滤
	distribute-list  route-map FilterCon out connected  //在分发列表中进行路由协议间过滤.
	
```



**更改指定路由条目的AD值**

```
OSPF:
access-list 2 permit 3.3.3.3   //ACL指定路由条目
router ospf 1
 router-id 1.1.1.1
 network 13.1.1.1 0.0.0.0 area 0
 distance 180 3.3.3.3 0.0.0.0 2   //OSPF指定邻居的RID
 
EIGRP:
access-list 1 permit 2.2.2.2  //ACL指定路由条目
router eigrp 100
 network 12.1.1.1 0.0.0.0
 distance 150 12.1.1.2 0.0.0.0 1  //EIGRP指定邻居的接口地址

```

**PBR**

```
ip access-list extended ACL1
 permit ip host 5.5.5.5 host 1.1.1.1
ip access-list extended ACL2
 permit ip host 55.55.55.55 host 1.1.1.1
route-map PBR permit 10
 match ip address ACL1
 set ip next-hop 10.1.1.2
route-map PBR permit 20
 match ip address ACL2
 set ip next-hop 10.1.1.3

interface e0/1
	ip policy route-map PBR  //接口调用Route-map
ip local policy route-map PBR   //全局配置模式下输入该命令, 路由器本身产生的流量,按照本策略转发.
```

**SLA**

```
ip sla 1   //定义一个SLA,用户探测远端连通性
 icmp-echo 12.1.1.2
 threshold 2000
 timeout 2000
 frequency 3
 
ip sla schedule 1 life forever start-time now  //定义SLA计划

track 11 ip sla 1 reachability  //创建一个track用于跟踪SLA状态
ip route 0.0.0.0 0.0.0.0 12.1.1.2 track 11  //定义一个主用默认路由, 并根据track状态进行浮动
ip route 0.0.0.0 0.0.0.0 13.1.1.3 2   //修改AD值为2, 设定备用默认路由.

show ip sla statistics  //查看sla统计结果 
show track 11  //查看track状态
```

## BGP

```
router bgp 234
 network 3.3.3.3 mask 255.255.255.255 route-map SetLoc   //通告网络,并利用route-map设置属性
 network 4.4.4.4 mask 255.255.255.255  //通告网络
 neighbor 3.3.3.3 remote-as 234  //构建IBGP邻居
 neighbor 3.3.3.3 update-source Loopback0  //指定IBGP邻居更新源
 neighbor 3.3.3.3 next-hop-self  //向指定邻居发送更新时, 修改一跳地址为自己
 neighbor 12.1.1.1 remote-as 1   //构建EBGP邻居
 
show ip bgp summary  //查看BGP邻居
show ip bgp  //查看BGP表
show ip route //查看路由表

clear ip bgp 2.2.2.2 soft in  //要求2.2.2.2重新发送一次更新
clear ip bgp 2.2.2.2 soft out  //向2.2.2.2 重新发送一次更新
clear ip bgp 2.2.2.2  //重置和2.2.2.2 的邻居关系
#可以用*代表所有邻居.
```



**BGP选路**

```
Weight影响选路:

router bgp 1
	neighbor 12.1.1.2  weight 3  //所有从该邻居学到的路由,weight设置3
	neighbor 12.1.1.2 route-map SetWei in  //通过Route-map 设置指定的路由条目的属性.
-----------
ip prefix-list PRE1 seq 5 permit 8.8.1.0/24

route-map SetWei permit 10
 match ip address prefix-list PRE1
 set weight 3
route-map SetWei permit 20
---------------


Local_preference影响选路:

router bgp 1
	 bgp default local-preference 150  //设置默认Local_preference为150.
	  neighbor 12.1.1.2 route-map SetLoc in  //通过route-map修改指定路由的Loc_pre
	  
route-map SetLoc permit 10
 match ip address prefix-list PRE1
 set local-preference 110
route-map SetLoc permit 20

AS-Path:

route-map SetAS permit 10
 set as-path prepend 22 33 44 55   //在as-path中添加指定号码
 
route-Map SetAS2 permit 10 
 set as-path prepend last-as  4 //重复as-path中最后一个as号N次

router bgp 78
 neighbor 48.1.1.4 route-map SetAS out  //出方向使用Route-map修改as-path


Router-ID:
router bgp 1
	bgp best path compare-routerid  //跳过第10条, 直接比较RID选路

router bgp 2
	bgp router-id  22.22.22.22 //修改BGP RID

MED:
router bgp 78
	bgp always-compare-med    //允许不同AS比较MED
	bgp bestpath med missing-as-worst   //当路由没有MED属性时, 按最差的MED值计算
	
```

**Peer-Group**

```
router bgp 345
 neighbor IBGP peer-group  //创建一个Peer-Group 名为 IBGP
 neighbor IBGP remote-as 345   //对IBGP这个PeerGroup做操作
 neighbor IBGP update-source Loopback0
 neighbor IBGP next-hop-self
 neighbor 4.4.4.4 peer-group IBGP   //将邻居加入IBGP这个组
 neighbor 5.5.5.5 peer-group IBGP
```



#### BGP路由过滤

**使用分发列表**

```
router bgp 1
	distribute-list ACL|Prefix-list   in|out
```

**使用前缀列表**

```
ip prefix-list PRE2 seq 5 deny 8.8.8.0/24
ip prefix-list PRE2 seq 10 permit 0.0.0.0/0 le 32

router bgp 1
	 neighbor 12.1.1.2 prefix-list PRE2 in   //入向引用前缀列表
```

**使用Route-Map过滤**

```
route-map Filter deny 10
	match metric 22
route-map Filter permit 20

router bgp 1
	neighbor 12.1.1.2 route-map Filter in   //入向引用route-map

```



**as-path list **

```
ip as-path access-list 1 deny ^2(_.*)?_78$    //过滤AS2开头  AS78结尾, 中间任意的路由.
ip as-path access-list 1 permit .*   //放行所有

router bgp 1
	 neighbor 12.1.1.2 filter-list 1 in   //入向引用as-path access-list 
```



# CCNP 交换

**PoE**

```
interface g1/0/1
	power inline {auto|never}  //开启/关闭PoE
show power inline  //查看poe供电信息
```



## SVI

**原理**

![](https://ws1.sinaimg.cn/large/006tNc79ly1fz3mq875cyj31oi0okguv.jpg)

**配置**

```
vlan 10,20  #创建vlan
interface e0/0   #接口划入VLAN
	switchport mode access
	switchport access vlan 10
interface vlan 10    #配置SVI接口
	no shutdown 
	ip address 10.1.1.3 255.255.255.0 
```

* SVI对应的VLAN必须提前存在

* SVI是三层接口,默认关闭,需要手工no shutdow

* SVI所属的VLAN下至少存在一个接口(含Trunk)

  `switchport auto-state exclude`  #接口自动状态排除

* 三层交换默认路由功能关闭,需要`ip routing` 手工开始路由功能

**逻辑拓扑**

![](https://ws1.sinaimg.cn/large/006tNc79ly1fz3q4ej2quj31ic0u0gsi.jpg)



## EtherChannel

**配置**

```
interface range e0/0-1
	channel-group 1 mode on  #手工捆绑接口到组1
interface port-channel 1   #进入port-channel接口配置模式
	switchport trunk encapsulation dot1q
	switchport mode trunk 
	
show etherchannel summary  #查看port-channel状态
```

**作业**

自学 LACP 和 PAgP动态EtherChannel协议.

## STP

**STP标准**

![](https://ws1.sinaimg.cn/large/006tNc79ly1fz4rmouzyxj319w0e4qe5.jpg)

**配置**

```
show spanning-tree [vlan N]  //查看指定生成树实例信息

spanning-tree vlan 1 priority 0  //修改交换机对于指定VLAN的优先级
panning-tree vlan 2 priority 4096 
或
spanning-tree vlan 1 root primary 
spanning-tree vlan 2 root secondary

spanning-tree vlan 1 cost 220  //修改指定VLAN指定接口的COST
spanning-tree vlan 1 port-priority 64  //修改接口优先级

spanning-tree portfast edge [trunk]  //在指定接口开启portfast
spanning-tree portfast default  //全局配置模式下开启portfast

spanning-tree bpduguard enable //接口下开启BPDU Guard功能
spanning-tree portfast edge bpduguard default  //全局配置下针对portfast接口开启BPDU Guard功能

spanning-tree bpdufilter enable //接口下开启BPDU filter
spanning-tree portfast edge bpdufilter default //全局配置下针对portfast接口开启BPDU filter

spanning-tree guard root //接口下开启Root Guard功能
spanning-tree guard loop //接口下开启Loop Guard功能

udld {enable | aggressive}  //全局开启UDLD
udld port [aggressive]  //接口开启UDLD
show udld //查看接口udld状态
udld reset  //重置UDLD err-disable接口状态
```

**STP保护机制示意**

![image-20190113113109565](https://ws2.sinaimg.cn/large/006tNc79ly1fz4st0ezrzj31ct0u0wug.jpg)

**MST**

```
spanning-tree mode mst   //切换到MST模式
spanning-tree mst configuration //进行mst子配置模式
 name CCNP   //命名
 revision 123   //定义revision号
 instance 1 vlan 1-2   //创建实例,关联vlan列表
 instance 2 vlan 3-4	//创建实例,关联vlan列表

spanning-tree mst 1 root primary 
spanning-tree mst 2 root secondary

spanning-tree mst 1 cost 220 
spanning-tree mst 1 port-priority 64 
```



## DHCP

**中继**

```
interface e0/0
	ip heler-address 10.1.1.253  #指定DHCP server地址
```



# HA

**NTP**

端口: UDP/123

```
#Server:
ntp authentication-key 1 md5 123456  //配置NTP密钥
ntp authenticate   //启用认证
ntp trusted-key 1   //使用ID为1的Key
ntp master 1    //启用NTP服务, 时间层级为1.

#Client
ntp authentication-key 1 md5 123456
ntp authenticate
ntp trusted-key 1
ntp server 1.1.1.1 key 1   //设置NTP server地址, 以及指定Key

ntp source loopback0  //指定NTP通信源地址

show ntp status  //在客户端查看NTP时间同步状态
```



**SNMP**

[Cisco SNMP Tools](http://tools.cisco.com/Support/SNMP/do/BrowseOID.do?local=en&substep=2&translate=Translate&tree=NO)

```
# 配置设备地点和联系人等附加信息
snmp-server location DLSP,LN,CN
snmp-server contact z@cciex.com
#定义view, 将OID关联到view
snmp-server view V1 ip included
snmp-server view V1 ospf included
snmp-server view V2 internet included
snmp-server view V2 tcp excluded
#创建组, 为组赋予view权限
snmp-server group G1 v3 priv read V1 write V2 
#在指定组内创建用户, 并设置用户认证和加密密钥
snmp-server user alice G1 v3 auth md5 cisco123 priv des cisco456
#设置trap
snmp-server host 172.16.60.88 version 3 auth alice  ospf eigrp
```

**SLA**

```
#定义一个ICMP echo探测, 无需在destination端配置responder
ip sla 1
 icmp-echo 23.1.1.3 source-interface Ethernet0/0
 timeout 2000   //超时
 frequency 5    //发包频率
ip sla schedule 1 life forever start-time now  //定义一个探测计划
#查看探测统计信息
show ip sla statistics 1 details 
```

```
#SLA source端配置udp-jitter测试
ip sla 2
 udp-jitter 23.1.1.3 16384
ip sla schedule 2 life forever start-time now

#SLA destination端需要配置responder
ip sla responder 

#source端查看探测统计信息
show ip sla statistics 1 details 
```

**SPAN**

```
monitor session 1 source interface Et0/0 rx
monitor session 1 destination interface Et0/1
```

**RSPAN**

```
#在相关交换上创建远程vlan, 交换机之间设置trunk
vlan 999
	remote-span 
	
#SRC_SWITCH
monitor session 1 source interface Et0/0 rx
monitor session 1 destination remote vlan 999

#DEST_SWITCH
monitor session 1 source remote vlan 999 
monitor session 1 destination interface Et0/0
```

[IOL L2 Image](https://pan.baidu.com/s/1VoPngGd3XLm8jJ1-nLN6Hg)

**堆叠**

```
show switch  //查看堆叠状态信息
switch N priority {1-15}  //设置本设备的优先级
switch N renumber M  //为交换机重新设置编号
```

**VSS**

[VSS configuration](http://blog.51cto.com/willy/1618831)

**SYSLOG**

```
[no] logging console  //打开或关闭Console线路的系统日志 (默认打开)

#VTY线路开启日志
who //确定自己所在线路
line N
	monitor  //激活该线路的syslog
	
#开启本地缓存日志, 设置缓存尺寸和日志级别
logging buffered 102400 5
#查看缓存区的日志
show logging  

#定义日志server信息
logging host 10.1.1.4 transport udp port 514 
```

# FHRP

## HSRP

```
interface Ethernet0/0
 ip address 10.1.1.1 255.255.255.0  //基本接口地址需要配置
 standby 10 ip 10.1.1.254  //配置vIP和组编号 
 standby 10 timers msec 500 2   //配置hello和hold时间
 standby 10 priority 200  //配置优先级
 standby 10 preempt    //开启角色抢占功能

show standby  //查看HSRP状态和配置信息

```

**上行接口跟踪**

```
#定义一个track 跟踪上行接口的line-protocol
track 11 interface e0/1 line-protocol 
#在HSRP接口下调用track, 当发生问题时,降低优先级, 以便让出active角色.
interface e0/0
	 standby 10 track 11 decrement 80  
```

**使用SLA进行更远节点探测**

```ip sla 12
#定义SLA进行远端地址探测
ip sla 12
 icmp-echo 13.1.1.3 source-interface Ethernet0/1
 threshold 2000
 timeout 2000
 frequency 5
ip sla schedule 12 life forever start-time now
#定义Track对SLA可达性进行跟踪
track 22 ip sla 12 reachability
#HSRP配置监控Track状态, 进行优先级调整.
interface e0/0
	 standby 10 track 22 decrement 80
	 
```



### VRRP

Hold =  Hello * 3 + Skew

Skew =  $\frac{256-priority}{256}​$



**HSRP vs VRRP**

![image-20190120132444837](https://ws2.sinaimg.cn/large/006tNc79ly1fzczfe83ghj31jw0oahdt.jpg)



**GLBP**

```
interface Ethernet0/0
 ip address 10.1.1.1 255.255.255.0
 glbp 10 ip 10.1.1.254
 glbp 10 timers 3 8
 glbp 10 priority 200
 glbp 10 preempt
 glbp 10 weighting 100 lower 50 upper 80
 glbp 10 load-balancing weighted
 #设置track, 当前track对象down时, 降低指定的权重
 glbp 10 weighting track 11 decrement 30 
 
 show glbp  //查看GLBP信息
```

## 安全基础



```
username xxxx   secret   xxx  //使用哈希方式设置密码, 不要使用明文
service password-encryption  //对明文密码加密 (仅防肩窥攻击)
banner login/exec/...   //设置不同类型的banner
no ip http server  //关闭http服务
no ip http secure server //关闭https服务
```

**Err-Disable**

```
errdisable recovery cause link-flap
errdisable recovery cause psecure-violation
errdisable recovery interval 500
```

**VACL**

```
mac access-list extended MACL
 permit host 0000.1111.1111 host 0000.2222.2222
 
ip access-list extended IPACL
 permit ip host 3.3.3.33 host 4.4.4.4

vlan access-map VMAP 1
 match mac address MACL
 action forward
vlan access-map VMAP 2
 match ip address IPACL
 action drop log
 
vlan filter VMAP vlan-list 10,20,30
```

**Storm-control**

```
 storm-control broadcast level pps 50k 20k
 storm-control multicast level bps 30m 10m
 storm-control unicast level 50.00 30.00
 storm-control action shutdown
 storm-control action trap
```



**命令级别**

```
privilege interface level 10 shutdown
privilege interface level 10 ip address
privilege interface level 10 ip
privilege configure level 10 interface
privilege exec level 10 configure terminal
privilege exec level 10 configure
privilege exec level 5 show ip route
privilege exec level 5 show ip
privilege exec level 10 show running-config
privilege exec level 5 show
```

**命令视图**

```
parser view View1
 secret 5 $1$UDxq$oLajvV0RiIW6Evrm0wX5d0
 commands configure include interface
 commands exec include configure terminal
 commands exec include configure
 commands exec include show running-config
 commands exec include show
```



**AAA**

```
#创建本地用户
username alice privilege 15 password 0 123

#开启AAA功能
aaa new-model

#创建一个Tacacs+服务器
tacacs server TS1
 #配置服务器IP
 address ipv4 10.70.1.112
 #配置和AAA服务器对接密码
 key cisco123
 
#创建另一个Tacacs+服务器
tacacs server TS2
 address ipv4 10.70.1.113
 key cisco123

#创建一个Tacacs+服务器组
aaa group server tacacs+ TG
 #将之前创建的服务器添加到该组
 server name TS1
 server name TS2

#创建一个认证列表,名称为"VTY_Authen" 认证方式为Tacacs+服务器组"TG"
aaa authentication login VTY_Authen group TG
#创建一个认证列表,名称为"CON_Authen" 认证方式为本地用户认证
aaa authentication login CON_Authen local

#创建一个基于级别的授权列表,名称为"VTY_Author_Exec" 授权Server组是TG
aaa authorization exec VTY_Author_Exec  group TG 

#创建一个命令集合的授权列表,名称为"VTY_Author_Command_15", 服务器为TG
aaa authorization commands 15 VTY_Author_Command_15 group TG

#让用户在配置模式下也受到命令授权的限制 
aaa authorization config-commands 

#创建一个Exec的审计列表, 名称为"VTY_Acc_Exec"
aaa accounting exec VTY_Acc_Exec start-stop group TG 
#创建一个command 15级的命令审计列表, 名称为"VTY_Acc_Command_15"
aaa accounting commands 15 VTY_Acc_Command_15 start-stop group TG

line vty 0 4
 #在VTY中调用认证方法列表"VTY_Authen"
 login authentication VTY_Authen
 #VTY使用AAA外部级别授权,调用授权列表"VTY_Author_Exec"
 authorization exec VTY_Author_Exec 
 ##VTY使用AAA外部命令授权,调用授权列表"VTY_Author_Command_15"
 authorization commands 15 VTY_Author_Command_15
 #VTY调用Exec审计列表"VTY_Acc_Exec"
 accounting exec VTY_Acc_Exec 
 #VTY调用Command 15级审计列表"VTY_Acc_Command_15"
 accounting commands 15 VTY_Acc_Command_15 
 transport input ssh
 
line con 0
 #在Console中调用认证方法列表"CON_Authen"
 login authentication CON_Authen
```



**DHCP_Snooping**

```
#全局开启DHCP_Snooping功能
ip dhcp snooping
#VLAN1开启DHCP_Snooping功能
ip dhcp snooping vlan 1
#交换机关闭option插入
no ip dhcp snooping information option  
#开启DHCP CHADDR和Ethernet 源mac地址一致性检查
ip dhcp snooping verify mac-address 
#设置DB的保存路径
ip dhcp snooping database unix:/dhcp.db
#设置DB存盘周期
ip dhcp snooping database write-delay 100
#设置DB写入超时时间
ip dhcp snooping database timeout 5
#手工写入DB条目(在特权模式下写入, 不会保存到running-config)
ip dhcp  snooping binding 0000.9999.9999 vlan 1 10.1.1.99 interface e0/3 expiry 4294967295     

interface e0/0
	#为接口设置为信任接口
	ip dhcp  snooping trust  
interface e0/1
	#为非信任接口设置速率限制
	ip dhcp  snooping limit rate 8 

show ip dhcp snooping  binding 
```

**DAI**

```
ip arp inspection vlan 1
interface e0/3 
	ip arp  inspection trust 
interface e0/1
	p arp inspection limit rate 88
```

**IP Source Guard**

```
interface e0/1
	ip verify source port-security 
```

**PVLAN**

```
vtp mode transparent 

vlan 10
  private-vlan primary
  private-vlan association 101-102
vlan 101
  private-vlan community
vlan 102
  private-vlan isolated

#将接口划入子VLAN
interface Ethernet0/0
 switchport private-vlan host-association 10 101
 switchport mode private-vlan host
#将接口设置为混杂接口, 并关联主vlan下的子VLANs
interface Ethernet1/0
 switchport private-vlan mapping 10 101-102
 switchport mode private-vlan promiscuous
```

